import { createGlobalStyle } from "styled-components";
import React from "react";
import Font from "./font/ttf/The1Official_Regular.ttf";
import Home from "./home";

// @font-face {
//   font-family: 'The1Official_Regular';
//   src: url(${Font}) format('truetype');
//   font-weight: normal;
//   font-style: normal;
// }
const GlobalStyle = createGlobalStyle`
@font-face {
  font-family: 'The1Official_Regular';
  src: url(${Font});
}
body {
  color: red;
  margin:5px !important
}
`;

function App() {
  return (
    <React.Fragment>
      <Home />
      <GlobalStyle />
    </React.Fragment>
  );
}

export default App;
